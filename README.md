# SimClient

SimClient is a Java application that represents the client component of the SiMessenger project. The SimClient is intended to function with the SimServer component,
the server, to complete the client/server architecture in which SiMessenger is comprised.

SimClient consists of two interfaces, a Command Line Interface (CLI) and a Graphical User Interface (GUI). Each interface is equally capable for executing all functions.
Additionally, each interface requires SimServer, the server component, to be running on a machine on the same local network as the client(s) in order to function completely.

## Installation

The project utilizes Apache Maven(https://maven.apache.org/) for dependency management and to build an executable jar.
For maven integration with eclipse a minimum version of Mars 4.5.1 is recommended.
After cloning the repository and importing the project into eclipse executing the following command will produce a jar:

mvn clean build

If eclipse is being used jars can also be produced by utilizing the interface and doing the following:
Right click the project -> Run As -> Maven Install

Three jars will be produced in the target directory of the project:

simclient-1.0.0-CLI-jar-with-dependencies.jar - executable and will launch the command line interface when run
simclient-1.0.0-GUI-jar-with-dependencies.jar - executable and will launch the graphical user interface when run
simclient-1.0.0.jar - not executable and will contain the project source code

## Usage
Assuming the executable for the SimServer is running on a server on the same local network, the client applications are used as follows:

Command Line Interface:

1.  launch the simclient-1.0.0-CLI-jar-with-dependencies.jar application
2.  Enter the server hostname (e.g. 'localhost' or '192.168.1.100'), when prompted.
3.  Enter a unique username, relative to other user currently conencted to the same SimServer application
4.  Ensure server identification and user authentication is successful. Re-enter information as prompted, if necessary.
5.  List all possible commands with the help command by typing 'help'
6.  Retrieve a list of all conversations on the connected server by using the command 'directory'
7.  Re-enter the 'directory' command as necessary to refresh the available conversations for which to contribute.
8.  Identify a conversation by the numerical number (e.g. 1 or 2), and send a message to the user in that conversation by using the send
       command as follows: 'send [message] [numerical conversation identifier]'
9.  Send a message containing spaces to a conversation by surrounding the message text in double quotes.
10. Repeat the above commands as desired.
11. To exit and disconnect from the server, type the 'exit' command.

Graphical User Interface

1.  Launch the simclient-1.0.0-GUI-jar-with-dependencies.jar application
2. Click Login 
3. Enter the server hostname (e.g. 'localhost' or '192.168.1.100'), when prompted.
4.  Enter a unique username, relative to other user currently conencted to the same SimServer application
5.  Ensure server identification and user authentication is successful. Re-enter information as prompted, if necessary.
6.  Retrieve a list of all conversations on the connected server by clicking the  'Conversation button'
7.  Re-enter the 'directory' command as necessary to refresh the available conversations for which to contribute.
8.  Identify a user by the numerical number (e.g. 1 or 2),  
9. Click the user number
10. Type a message and click the ‘send’ button
11.  The sent message will be displayed in the user number tab
12. The received message will display in the user tab
13. To exit close the window

## History

1.0 First release

## Credits
Lindsey Young, Dan Ford, Greg Warchol, Ken Balogh
package simclient.conversation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import simcommon.conversation.Conversation;

public class IncrementalIntegerConversationAssociater implements IConversationAssociater{
	
	public Conversation getConversation(Collection<Conversation> conversations, String identifier){
		try{
			return sortConversations(conversations).get(Integer.parseInt(identifier) - 1);
		}catch(Exception ex){
			return null;
		}
	}
	
	public Map<String, Conversation> associateConversations(Collection<Conversation> conversations){
		ArrayList<Conversation> sortedConversations = sortConversations(conversations);
		Map<String, Conversation> result = new HashMap<String, Conversation>();
		int key = 1;
		for(Conversation conversation : sortedConversations){
			result.put(String.valueOf(key), conversation);
			key++;
		}
		return result;
	}
	
	public String resolveConversationToIdentifier(Collection<Conversation> conversations, Conversation targetConversation){
		Map<String, Conversation> associatedConversations = associateConversations(conversations);
		for(Map.Entry<String, Conversation> conv : associatedConversations.entrySet()){
			if(conv.getValue().equals(targetConversation)){
				return conv.getKey();
			}
		}
		return null;
	}
	
	private ArrayList<Conversation> sortConversations(Collection<Conversation> conversations){
		ArrayList<Conversation> sorted = new ArrayList<Conversation>(conversations);
		sorted.sort(new Comparator<Conversation>(){
			@Override
			public int compare(Conversation conv0, Conversation conv1) {
				return conv0.getConversationId().hashCode() > conv1.getConversationId().hashCode() ? 0 : 1;
			}
		});
		return sorted;
	}
}

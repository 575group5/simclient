package simclient.conversation;

import java.util.ArrayList;

import simcommon.conversation.Conversation;
import simcommon.conversation.IConversationIdentifier;
import simcommon.entities.User;

public class SingleSenderConversationIdentifier implements IConversationIdentifier {

	@Override
	public String getConversationIdentifier(Conversation conversation) {
		try{
			ArrayList<User> users = new ArrayList<User>(conversation.getParticipants());
			return "(" + users.get(0).getUserInfo().getName() + ", " + users.get(1).getUserInfo().getName() + ")";
		} catch(Exception ex){
			return "Unknown";
		}
	}

}

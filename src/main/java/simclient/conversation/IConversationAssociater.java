package simclient.conversation;

import java.util.Collection;
import java.util.Map;

import simcommon.conversation.Conversation;

public interface IConversationAssociater {

	public Conversation getConversation(Collection<Conversation> conversations, String identifier);
	
	public Map<String, Conversation> associateConversations(Collection<Conversation> conversations);
	
	public String resolveConversationToIdentifier(Collection<Conversation> conversations, Conversation targetConversation);
}

package simclient.authentication;

import simcommon.authentication.IUserAuthentication;

public interface IAuthenticationObtainedCallback {

	void onAuthenticationObtained(IUserAuthentication authMechanism);
}

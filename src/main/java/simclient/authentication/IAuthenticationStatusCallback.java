package simclient.authentication;

import simcommon.FailReason;

public interface IAuthenticationStatusCallback {
	void onAttemptingToAuthenticate();
	void onAuthenticated();
	void onNotAuthenticated(FailReason failReason);
}

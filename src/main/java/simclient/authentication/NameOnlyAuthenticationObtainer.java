package simclient.authentication;

import simcommon.authentication.UsernameAuthentication;

public abstract class NameOnlyAuthenticationObtainer implements IAuthenticationObtainer {
	
	public void obtainAuthentication(IAuthenticationObtainedCallback callback){
		obtainUsername(new IUsernameObtainedCallback() {
			public void onUsernameObtained(String username) {
				callback.onAuthenticationObtained(new UsernameAuthentication(username));
			}
		});
	}
	
	public abstract void obtainUsername(IUsernameObtainedCallback callback);
}

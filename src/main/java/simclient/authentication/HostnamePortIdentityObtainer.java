package simclient.authentication;

import simcommon.identification.HostnamePortIdentity;
import simcommon.identification.IHostnameObtainedCallback;
import simcommon.identification.IHostnameObtainer;
import simcommon.identification.IPortObtainedCallback;
import simcommon.identification.IPortObtainer;
import simcommon.identification.IServerIdentityObtainedCallback;
import simcommon.identification.IServerIdentityObtainer;
import simcommon.utilities.ParamUtil;

public class HostnamePortIdentityObtainer implements IServerIdentityObtainer {

	protected IHostnameObtainer mHostnameObtainer;
	protected IPortObtainer mPortObtainer;
	
	public HostnamePortIdentityObtainer(IHostnameObtainer hostObtainer, IPortObtainer portObtainer){
		ParamUtil.validateParamNotNull("Host name obtainer must be supplied.", hostObtainer);
		ParamUtil.validateParamNotNull("Port obtainer must be supplied.", portObtainer);
		mHostnameObtainer = hostObtainer;
		mPortObtainer = portObtainer;
	}
	
	@Override
	public void obtainServerIdentity(IServerIdentityObtainedCallback callback) {
		mHostnameObtainer.obtainHostname(new IHostnameObtainedCallback() {
		    public void onHostnameObtained(String hostname) {
		             mPortObtainer.obtainPort(new IPortObtainedCallback() {
		                   public void onPortObtained(int port) {
		                        HostnamePortIdentity serverIdentity = new HostnamePortIdentity(hostname, port);
		                         callback.onServerIdentityObtained(serverIdentity);
		                   }
		             });
		     }
		});
		
	}
}
package simclient.authentication;

public interface IUsernameObtainedCallback {

	void onUsernameObtained(String username);
}

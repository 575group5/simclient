package simclient.authentication;

public interface IAuthenticationObtainer {

	void obtainAuthentication(IAuthenticationObtainedCallback callback);
}

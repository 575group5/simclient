package simclient.cli.commandhandler;

import simclient.client.ClientCore;
import simclient.commandmessage.ICommandProperties;
import simclient.textresolve.LanguageMap;
import simcommon.utilities.ParamUtil;

public class UnknownCommandHandler extends CommandHandler  {

	private ICommandProperties mUnknownCommandProperties;
	
	public UnknownCommandHandler(LanguageMap languageMap, ICommandProperties commandProperties) {
		super(languageMap);
		ParamUtil.validateParamNotNull("Properties for the command must be supplied.", commandProperties);
		mUnknownCommandProperties = commandProperties;
	}

	public void handleCommand(String userInput, ClientCore clientCore) {
		System.out.println(mUnknownCommandProperties.commandUsage());
	}

}
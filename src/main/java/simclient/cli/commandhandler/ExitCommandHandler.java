package simclient.cli.commandhandler;

import simclient.client.ClientCore;
import simclient.textresolve.LanguageMap;

public class ExitCommandHandler extends CommandHandler  {
	
	public ExitCommandHandler(LanguageMap languageMap) {
		super(languageMap);
	}

	public void handleCommand(String userInput, ClientCore clientCore) {
		clientCore.stop();
		System.out.println("Client stopped");
	}

}

package simclient.cli.commandhandler;

import java.util.HashMap;
import java.util.Map;

import simclient.cli.commandresolve.Command;

public class CommandHandlerSelector {
	private Map<Command, CommandHandler> handlers;
	
	public CommandHandlerSelector(){
		handlers = new HashMap<Command, CommandHandler>();
	}
	
	public void addHandler(Command command, CommandHandler handler){
		handlers.put(command, handler);
	}
	public CommandHandler getCommandHandler(Command command){
		return handlers.get(command);
	}
}
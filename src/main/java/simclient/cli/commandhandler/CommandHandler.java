package simclient.cli.commandhandler;

import simclient.client.ClientCore;
import simclient.textresolve.LanguageMap;
import simcommon.utilities.ParamUtil;

public abstract class CommandHandler {

	protected LanguageMap mLanguageMap;
	
	public CommandHandler(LanguageMap languageMap){
		ParamUtil.validateParamNotNull("A language map must be supplied.", languageMap);
		mLanguageMap = languageMap;
	}
	
	public abstract void handleCommand(String userInput, ClientCore clientCore);
	
	protected LanguageMap getLanguageMap(){
		return mLanguageMap;
	}
}
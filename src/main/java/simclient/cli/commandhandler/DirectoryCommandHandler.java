package simclient.cli.commandhandler;

import simclient.client.ClientCore;
import simclient.textresolve.LanguageMap;

public class DirectoryCommandHandler extends CommandHandler  {

	public DirectoryCommandHandler(LanguageMap languageMap) {
		super(languageMap);
	}

	public void handleCommand(String userInput, ClientCore clientCore) {
		clientCore.requestConversationList();
	}

}
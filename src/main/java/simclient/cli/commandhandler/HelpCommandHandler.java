package simclient.cli.commandhandler;

import java.util.Map;

import simclient.cli.commandresolve.Command;
import simclient.cli.commandresolve.CommandMap;
import simclient.client.ClientCore;
import simclient.commandmessage.ICommandProperties;
import simclient.textresolve.LanguageMap;
import simcommon.utilities.ParamUtil;

public class HelpCommandHandler extends CommandHandler  {

	private CommandMap mCommandMap;
	
	public HelpCommandHandler(LanguageMap languageMap, CommandMap commandMap) {
		super(languageMap);
		ParamUtil.validateParamNotNull("A Command Map must be supplied.", commandMap);
		mCommandMap = commandMap;
	}

	public void handleCommand(String userInput, ClientCore clientCore) {
		for (Map.Entry<Command, ICommandProperties> entry : getCommandMap().getCommandMap().entrySet()) {
			if(entry.getValue().commandName() != null)
				System.out.printf("%-20s %s \n", entry.getValue().commandName(), entry.getValue().commandHelp());
		}
	}

	private CommandMap getCommandMap(){
		return mCommandMap;
	}
}

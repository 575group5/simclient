package simclient.cli.commandhandler;

import java.util.List;

import simclient.cli.utilities.CommandParser;
import simclient.client.ClientCore;
import simclient.client.listeners.ISentMessageStatusListener;
import simclient.commandmessage.ICommandProperties;
import simclient.conversation.IConversationAssociater;
import simclient.textresolve.LanguageMap;
import simcommon.conversation.Conversation;
import simcommon.messages.userMessages.IUserMessage;
import simcommon.messages.userMessages.PlainAsciiUserMessage;

public class SendCommandHandler extends CommandHandler  {

	private String mMessage;
	private String mConversationName;
	private ICommandProperties mCommandProperties;
	private IConversationAssociater mConversationAssociater;
	
	public SendCommandHandler(LanguageMap languageMap, ICommandProperties commandProperties, IConversationAssociater associater) {
		super(languageMap);
		mCommandProperties = commandProperties;
		mConversationAssociater = associater;
	}

	@Override
	public void handleCommand(String userInput, ClientCore clientCore) {
		try{
			parseCommand(userInput);
		} catch(Exception ex){
			System.out.println(mCommandProperties.commandUsage());
			return;
		}
		
		Conversation conversation = mConversationAssociater.getConversation(clientCore.getConversations(),
				mConversationName);
		if(conversation == null){
			System.out.println("The selected conversation does not exist.");
			return;
		}
		
		clientCore.sendMessage(conversation, new PlainAsciiUserMessage(mMessage), new ISentMessageStatusListener() {
			@Override
			public void onMessageSendSuccessful(Conversation conversation, IUserMessage message) { }
			
			@Override
			public void onMessageSendFail(Conversation conversation, IUserMessage message) {
				System.out.println("The message to " + conversation.getName() + " failed to send.");
			}
		});
	}

	private void parseCommand(String userInput){
		List<String> args = CommandParser.ParseCommand(userInput);
		mMessage = args.get(1);
		mConversationName = args.get(2);
	}
}

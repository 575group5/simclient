package simclient.cli;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import simclient.authentication.HostnamePortIdentityObtainer;
import simclient.cli.authentication.CliNameOnlyAuthenticationObtainer;
import simclient.cli.commandresolve.Command;
import simclient.cli.listeners.CliConversationListReceivedListener;
import simclient.cli.listeners.CliPlainAsciiMessageReceivedListener;
import simclient.cli.listeners.CliServerIdentificationStatusListener;
import simclient.client.ClientCore;
import simclient.client.SimClientCore;
import simclient.client.listeners.IAuthenticationStatusListener;
import simclient.client.listeners.MessageReceivedDelegatingListener;
import simclient.conversation.IncrementalIntegerConversationAssociater;
import simclient.conversation.SingleSenderConversationIdentifier;
import simclient.identification.CliHostnameObtainer;
import simclient.identification.PredefinedPortObtainer;
import simclient.message.SingleUserMessageOriginIdentifier;
import simclient.textresolve.LanguageMap;
import simclient.textresolve.Text;
import simcommon.utilities.ParamUtil;

public class CliClient {

	private ClientCore mClientCore;
	private LanguageMap mTextGenerator;
	private CommandInterpreter mCommandInterpreter;
	private static boolean mCheckForCommands = false;
	private BufferedReader mInputReader;
	
	public CliClient(LanguageMap languageMap, CommandInterpreter interpreter){
		ParamUtil.validateParamNotNull("A language map must be supplied.", languageMap);
		ParamUtil.validateParamNotNull("A command interpreter must be supplied.", languageMap);
		mTextGenerator = languageMap;
		mCommandInterpreter = interpreter;
		mInputReader = createInputReader();
	}
	
	public int runClient() {
			System.out.println(getTextGenerator().getStringForText(Text.WELCOME_MESSAGE));
			
			connectClientCore(mInputReader);
			return 0;
	}
	
	private void stopProcessCommands(){
		mCheckForCommands = false;
	}
	
	private BufferedReader createInputReader(){
		InputStreamReader inputStreamReader = new InputStreamReader(System.in);
		return new BufferedReader(inputStreamReader);
	}
	
	protected LanguageMap getTextGenerator(){
		return mTextGenerator;
	}
	
	protected CommandInterpreter getCommandInterpreter(){
		return mCommandInterpreter;
	}
	
	private void processCommands(BufferedReader inputReader){
		new Thread(){
			@Override
			public void run(){
				String userInput = null;
				mCheckForCommands = true;
				System.out.println(getTextGenerator().getStringForText(Text.ENTER_COMMANDS_MESSAGE) + " " +
						getTextGenerator().getStringForText(Text.HOW_TO_QUIT_MESSAGE) + " '" + 
						getCommandInterpreter().lookupCommandProperties(Command.EXIT).commandName() + "'");
				
				while(mCheckForCommands && !getCommandInterpreter().programExitRequested(userInput))
				{
					try {
						if(inputReader.ready()){
							userInput = inputReader.readLine();
							getCommandInterpreter().processInput(userInput, mClientCore);
						}
						Thread.sleep(500);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}.start();
	}
	
	private void connectClientCore(BufferedReader inputReader){
		CliHostnameObtainer mHostnameObtainer = new CliHostnameObtainer(inputReader);
		PredefinedPortObtainer mPortObtainer = new PredefinedPortObtainer();
		HostnamePortIdentityObtainer hostObtainer = new HostnamePortIdentityObtainer(mHostnameObtainer, mPortObtainer);
		CliNameOnlyAuthenticationObtainer authObtainer = new CliNameOnlyAuthenticationObtainer(inputReader);
		
		mClientCore = new SimClientCore(hostObtainer, authObtainer);
		attachCoreListeners(inputReader);
		mClientCore.start();
	}
	
	private void attachCoreListeners(BufferedReader inputReader) {
		mClientCore.attachAuthenticationStatusListener(new IAuthenticationStatusListener() {

			@Override
			public void onAuthenticationCheckBegin() {
				stopProcessCommands();
			}

			@Override
			public void onAuthenticationSuccess() {
				System.out.println("Authentication successful!");
				processCommands(mInputReader);
			}

			@Override
			public void onAuthenticationFailure() {
				System.out.println("Authentication failed!");
			}

		});
		mClientCore.attachConversationListReceivedListener(new CliConversationListReceivedListener(new IncrementalIntegerConversationAssociater()));
		MessageReceivedDelegatingListener.Builder messageReceivedDelegatingListenerBuilder = new MessageReceivedDelegatingListener.Builder();
		messageReceivedDelegatingListenerBuilder.add(new CliPlainAsciiMessageReceivedListener(new SingleSenderConversationIdentifier(), new SingleUserMessageOriginIdentifier()));
		mClientCore.attachMessageReceivedListener(messageReceivedDelegatingListenerBuilder.build());
		mClientCore.attachServerIdentificationStatusListener(new CliServerIdentificationStatusListener());
	}
}

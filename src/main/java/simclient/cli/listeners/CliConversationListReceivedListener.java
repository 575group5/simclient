package simclient.cli.listeners;

import java.util.Collection;
import java.util.Map;

import simclient.client.listeners.IConversationListReceivedListener;
import simclient.conversation.IncrementalIntegerConversationAssociater;
import simcommon.conversation.Conversation;
import simcommon.conversation.ConversationProperties;
import simcommon.entities.User;
import simcommon.utilities.ParamUtil;

public class CliConversationListReceivedListener implements IConversationListReceivedListener {

	private IncrementalIntegerConversationAssociater mConversationAssociater;
	
	public CliConversationListReceivedListener(IncrementalIntegerConversationAssociater associater){
		ParamUtil.validateParamNotNull("Conversation Associator must be supplied.", associater);
		mConversationAssociater = associater;
	}
	
	@Override
	public void onConversationListReceived(Collection<Conversation> conversations) {
		
		if(conversations.isEmpty()){
			System.out.println("There are no conversations.");
			return;
		}
		
		Map<String, Conversation> convMap = mConversationAssociater.associateConversations(conversations);
		for(Map.Entry<String, Conversation> convAssociation : convMap.entrySet()){
			ConversationProperties conversationProperties = convAssociation.getValue().getProperties();
			System.out.println("Conversation: " + convAssociation.getKey());
			for (User user : conversationProperties.getParticipants()) {
				System.out.printf("%-5s \n", user.getUserInfo().getName());
			}
		}
	}
	
}
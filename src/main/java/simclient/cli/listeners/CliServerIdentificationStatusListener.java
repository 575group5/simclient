package simclient.cli.listeners;

import simclient.client.listeners.IServerIdentificationStatusListener;

public class CliServerIdentificationStatusListener implements IServerIdentificationStatusListener {

	@Override
	public void onServerIdentificationSuccess() {
		System.out.println("Server Identity obtained successfully!");
	}

	@Override
	public void onServerIdentificationFailure() {
		System.out.println("Could not obtain server identity.");
	}

}
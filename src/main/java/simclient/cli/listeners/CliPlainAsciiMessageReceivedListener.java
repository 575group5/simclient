package simclient.cli.listeners;

import simclient.client.listeners.SpecificUserMessageReceivedListener;
import simcommon.conversation.Conversation;
import simcommon.conversation.IConversationIdentifier;
import simcommon.messages.userMessages.IUserMessage;
import simcommon.messages.userMessages.PlainAsciiUserMessage;
import simcommon.messages.userMessages.origin.IUserMessageOrigin;
import simcommon.messages.userMessages.origin.IUserMessageOriginIdentifier;

public class CliPlainAsciiMessageReceivedListener extends SpecificUserMessageReceivedListener {

	public CliPlainAsciiMessageReceivedListener(IConversationIdentifier conversationIdentifier,
			IUserMessageOriginIdentifier originIdentifier) {
		super(conversationIdentifier, originIdentifier);
	}

	public void onMessageReceived(IUserMessageOrigin source, Conversation conversation, IUserMessage message) {
		System.out.println(mUserMessageOriginIdentifier.getOriginIdentifier(source) + 
				" from conversation " + mUserMessageConversationIdentifier.getConversationIdentifier(conversation) + 
				" says: " + getMesageContent(message));
	}
	
	private String getMesageContent(IUserMessage message){
		PlainAsciiUserMessage asciiMessage = (PlainAsciiUserMessage) message;
		return asciiMessage.getContent();
	}

	public boolean canHandle(IUserMessage message) {
		return (message instanceof PlainAsciiUserMessage);
	}
}

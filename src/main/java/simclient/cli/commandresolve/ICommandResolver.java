package simclient.cli.commandresolve;

public interface ICommandResolver {
	public String getCommandNameString(Command command);
	public Command resolveStringCommand(String commandString);
}

package simclient.cli.commandresolve;

import java.util.Map;
import java.util.Objects;

import simclient.commandmessage.ICommandProperties;

public abstract class CommandMap implements ICommandResolver {
	public abstract Map<Command, ICommandProperties> getCommandMap();
	public abstract String getCommandUsage(Command command);
	
	public String getCommandNameString(Command command) {
		return getCommandMap().get(command).commandName();
	}
	
	public Command resolveStringCommand(String commandString){
		for (Map.Entry<Command, ICommandProperties> entry : getCommandMap().entrySet()) {
		    if(Objects.equals(entry.getValue().commandName(), commandString))
		    	return entry.getKey();
		}
		return Command.UNKNOWN;
	}
	
	public ICommandProperties getCommandProperties(Command command){
		return getCommandMap().get(command);
	}
}

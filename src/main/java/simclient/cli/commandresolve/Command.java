package simclient.cli.commandresolve;

public enum Command {
	HELP,
	EXIT,
	DIRECTORY,
	SEND_MESSAGE,
	UNKNOWN
}

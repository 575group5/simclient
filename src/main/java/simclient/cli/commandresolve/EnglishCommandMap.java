package simclient.cli.commandresolve;

import java.util.HashMap;
import java.util.Map;

import simclient.commandmessage.*;
import simclient.textresolve.Text;

public class EnglishCommandMap extends CommandMap{

	private static final Map<Command, ICommandProperties> ENGLISH_COMMANDS;
	static {
		ENGLISH_COMMANDS = new HashMap<>(Text.values().length);
		ENGLISH_COMMANDS.put(Command.HELP, new EnglishHelpCommandMessage());
		ENGLISH_COMMANDS.put(Command.EXIT, new EnglishExitCommandMessage());
		ENGLISH_COMMANDS.put(Command.DIRECTORY, new EnglishDirectoryCommandMessage());
		ENGLISH_COMMANDS.put(Command.SEND_MESSAGE, new EnglishSendCommandMessage());
		ENGLISH_COMMANDS.put(Command.UNKNOWN, new EnglishUnknownCommandMessage());
	}
	
	@Override
	public Map<Command, ICommandProperties> getCommandMap() {
		return ENGLISH_COMMANDS;
	}

	public String getCommandUsage(Command command){
		for (Map.Entry<Command, ICommandProperties> entry : getCommandMap().entrySet()) {
			if(entry.getKey() == command)
				return entry.getValue().commandUsage();
		}
		return new EnglishUnknownCommandMessage().commandUsage();
	}
}

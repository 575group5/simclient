package simclient.cli.utilities;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommandParser {

	public static List<String> ParseCommand(String command){
		List<String> parsedCmds = new ArrayList<String>();
		Matcher match = Pattern.compile("([^\"]\\S*|\".+?\")\\s*").matcher(command);
		while (match.find())
			parsedCmds.add(match.group(1).replace("\"", ""));
		return parsedCmds;
	}
}

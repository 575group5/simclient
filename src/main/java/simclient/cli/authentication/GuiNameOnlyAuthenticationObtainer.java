package simclient.cli.authentication;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;

import javax.swing.JOptionPane;

import simclient.authentication.IUsernameObtainedCallback;
import simclient.authentication.NameOnlyAuthenticationObtainer;
import simclient.cli.utilities.CommandParser;
import simcommon.utilities.ParamUtil;

public class GuiNameOnlyAuthenticationObtainer extends NameOnlyAuthenticationObtainer {

	String userInputStr;
	
	public GuiNameOnlyAuthenticationObtainer(String userInput){
	
		userInputStr = userInput;
		System.out.println("Name input is:"+ userInputStr);
	}
	
	
	public void obtainUsername(IUsernameObtainedCallback callback) {
		 do{
			 userInputStr = JOptionPane.showInputDialog("Enter your username:","");
		 }while(userInputStr.equals("")); //returns null if canceled
		 if (userInputStr != null){
			 callback.onUsernameObtained(userInputStr);
		 }
	}

}

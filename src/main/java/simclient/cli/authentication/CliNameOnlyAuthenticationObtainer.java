package simclient.cli.authentication;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;

import simclient.authentication.NameOnlyAuthenticationObtainer;
import simclient.authentication.IUsernameObtainedCallback;
import simclient.cli.utilities.CommandParser;
import simcommon.utilities.ParamUtil;

public class CliNameOnlyAuthenticationObtainer extends NameOnlyAuthenticationObtainer {

	private BufferedReader mInputReader;
	
	public CliNameOnlyAuthenticationObtainer(BufferedReader inputReader){
		ParamUtil.validateParamNotNull("Reader for input must be supplied", inputReader);
		mInputReader = inputReader;
	}
	
	public void obtainUsername(IUsernameObtainedCallback callback) {
		System.out.print("Enter your username: ");
		String userInput;
		try {
			userInput = mInputReader.readLine();
		} catch (IOException e) {
			userInput = null;
		}
		List<String> args = CommandParser.ParseCommand(userInput);
		callback.onUsernameObtained(args.isEmpty() ? null : args.get(0));
	}

}
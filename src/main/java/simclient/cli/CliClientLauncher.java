package simclient.cli;

import simclient.cli.commandhandler.CommandHandlerSelector;
import simclient.cli.commandhandler.DirectoryCommandHandler;
import simclient.cli.commandhandler.ExitCommandHandler;
import simclient.cli.commandhandler.HelpCommandHandler;
import simclient.cli.commandhandler.SendCommandHandler;
import simclient.cli.commandhandler.UnknownCommandHandler;
import simclient.cli.commandresolve.Command;
import simclient.cli.commandresolve.CommandMap;
import simclient.cli.commandresolve.EnglishCommandMap;
import simclient.conversation.IncrementalIntegerConversationAssociater;
import simclient.textresolve.EnglishLanguageMap;
import simclient.textresolve.LanguageMap;
import simcommon.systemmessagecentral.IStatusCallback;

public class CliClientLauncher {
	
	private LanguageMap mLanguageMap;
	private CommandMap mCommandMap;
	private CliClient mClient;
	
	public int launchInterface(){
		mClient = new CliClient(getLanguageMap(), buildCliInterpreter());
		return mClient.runClient();
	}
	
	private LanguageMap getLanguageMap(){
		if(mLanguageMap == null)
			mLanguageMap = new EnglishLanguageMap();
		
		return mLanguageMap;
	}
	
	private CommandMap getCommandMap(){
		if(mCommandMap == null)
			mCommandMap = new EnglishCommandMap();
		
		return mCommandMap;
	}
	
	private CommandInterpreter buildCliInterpreter(){
		CommandHandlerSelector handlerSelector = buildHandlerSelector();
		return new CommandInterpreter(getLanguageMap(), getCommandMap(), handlerSelector);
	}
	
	private CommandHandlerSelector buildHandlerSelector(){
		CommandHandlerSelector handlerSelector = new CommandHandlerSelector();
		handlerSelector.addHandler(Command.SEND_MESSAGE, new SendCommandHandler(getLanguageMap(), getCommandMap().getCommandProperties(Command.SEND_MESSAGE), new IncrementalIntegerConversationAssociater()));
		handlerSelector.addHandler(Command.HELP, new HelpCommandHandler(getLanguageMap(), getCommandMap()));
		handlerSelector.addHandler(Command.DIRECTORY, new DirectoryCommandHandler(getLanguageMap()));
		handlerSelector.addHandler(Command.EXIT, new ExitCommandHandler(getLanguageMap()));
		handlerSelector.addHandler(Command.UNKNOWN, new UnknownCommandHandler(getLanguageMap(), getCommandMap().getCommandProperties(Command.UNKNOWN)));
		return handlerSelector;
	}

}
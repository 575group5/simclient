package simclient.cli;

import simclient.cli.commandhandler.*;
import simclient.cli.commandresolve.Command;
import simclient.cli.commandresolve.CommandMap;
import simclient.cli.utilities.CommandParser;
import simclient.client.ClientCore;
import simclient.commandmessage.ICommandProperties;
import simclient.textresolve.LanguageMap;
import simcommon.utilities.ParamUtil;

public class CommandInterpreter {

	private CommandMap mCommandMap;
	private LanguageMap mLanguageMap;
	private CommandHandlerSelector mHandlerSelector;
	
	public CommandInterpreter(LanguageMap languageMap, CommandMap commandMap, CommandHandlerSelector handlerSelector){
		ParamUtil.validateParamNotNull("Language map must be supplied.", languageMap);
		ParamUtil.validateParamNotNull("Command map must be supplied.", commandMap);
		ParamUtil.validateParamNotNull("A command handler selector must be supplied.", handlerSelector);
		mCommandMap = commandMap;
		mLanguageMap = languageMap;
		mHandlerSelector = handlerSelector;
	}
	
	public boolean programExitRequested(String userInput){
		return getCommandMap().resolveStringCommand(userInput) == Command.EXIT;
	}
	
	public ICommandProperties lookupCommandProperties(Command command){
		return getCommandMap().getCommandProperties(command);
	}
	
	public void processInput(String userInput, ClientCore clientCore){
		CommandHandler handler = mHandlerSelector.getCommandHandler(extractCommand(userInput));
		handler.handleCommand(userInput, clientCore);
	}
	
	private Command extractCommand(String userInput){
		String commandPart = (userInput != null && !userInput.isEmpty()) 
				? CommandParser.ParseCommand(userInput).get(0).toLowerCase() : null;
		return getCommandMap().resolveStringCommand(commandPart);
	}
	
	public CommandMap getCommandMap(){
		return mCommandMap;
	}
	
	public LanguageMap getLanguageMap(){
		return mLanguageMap;
	}
}
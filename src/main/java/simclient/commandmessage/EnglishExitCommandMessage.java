package simclient.commandmessage;

public class EnglishExitCommandMessage implements ICommandProperties {

	@Override
	public String commandName() {
		return "exit";
	}

	@Override
	public String commandHelp() {
		return "Exit the SiMessenger client.";
	}

	@Override
	public String commandUsage() {
		return "Exit command usage: " + commandName();
	}

}

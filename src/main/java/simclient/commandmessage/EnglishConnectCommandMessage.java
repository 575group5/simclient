package simclient.commandmessage;

public class EnglishConnectCommandMessage implements ICommandProperties{

	@Override
	public String commandName() {
		return "connect";
	}
	
	@Override
	public String commandHelp() {
		return "Enter the SiMessenger server hostname followed by your username.";
	}

	@Override
	public String commandUsage() {
		return "Connect command usage: [server hostname] [username]";
	}

}

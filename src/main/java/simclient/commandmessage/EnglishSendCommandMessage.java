package simclient.commandmessage;

public class EnglishSendCommandMessage implements ICommandProperties {

	@Override
	public String commandName() {
		return "send";
	}

	@Override
	public String commandHelp() {
		return "Send a specified message to a specified conversation.";
	}

	@Override
	public String commandUsage() {
		return "Send command usage: " + commandName() + " [message] [conversation]";
	}

}

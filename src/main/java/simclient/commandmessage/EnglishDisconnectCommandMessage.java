package simclient.commandmessage;

public class EnglishDisconnectCommandMessage implements ICommandProperties {

	@Override
	public String commandName() {
		return "disconnect";
	}

	@Override
	public String commandHelp() {
		return "Disconnect from the SiMessenger server.";
	}

	@Override
	public String commandUsage() {
		return "Disconnect command usage: " + commandName();
	}

}

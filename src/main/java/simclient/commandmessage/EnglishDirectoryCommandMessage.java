package simclient.commandmessage;

public class EnglishDirectoryCommandMessage implements ICommandProperties {

	@Override
	public String commandName() {
		return "directory";
	}

	@Override
	public String commandHelp() {
		return "List the available conversations for which you can contribute.";
	}

	@Override
	public String commandUsage() {
		return "Directory command usage: " + commandName();
	}

}

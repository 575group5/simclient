package simclient.commandmessage;

public interface ICommandProperties {

	public String commandName();
	public String commandHelp();
	public String commandUsage();
}

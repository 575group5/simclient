package simclient.commandmessage;

public class EnglishUnknownCommandMessage implements ICommandProperties {

	@Override
	public String commandName() {
		return null;
	}

	@Override
	public String commandHelp() {
		return "The command is not recognized.";
	}

	@Override
	public String commandUsage() {
		return "The command is not recognized";
	}

}
package simclient.commandmessage;

public class EnglishHelpCommandMessage implements ICommandProperties {

	@Override
	public String commandName() {
		return "help";
	}

	@Override
	public String commandHelp() {
		return "Get help about available commands.";
	}

	@Override
	public String commandUsage() {
		return "Help command usage: " + commandName();
	}

}

package simclient.identification;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;

import simclient.cli.utilities.CommandParser;
import simcommon.identification.IHostnameObtainedCallback;
import simcommon.identification.IHostnameObtainer;
import simcommon.utilities.ParamUtil;

public class CliHostnameObtainer implements IHostnameObtainer {
	
	private BufferedReader mInputReader;
	
	public CliHostnameObtainer(BufferedReader reader){
		ParamUtil.validateParamNotNull("Reader for input must be supplied", reader);
		mInputReader = reader;
	}
	
	public void obtainHostname(IHostnameObtainedCallback callback) {
		System.out.print("Enter SiMessenger hostname: ");
		String userInput;
		try {
			userInput = mInputReader.readLine();
		} catch (IOException e) {
			userInput = null;
		}
		List<String> args = CommandParser.ParseCommand(userInput);
		callback.onHostnameObtained(args.isEmpty() ? null : args.get(0));
	}
}

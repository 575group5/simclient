package simclient.identification;

import java.io.IOException;
import java.util.List;

import javax.swing.JOptionPane;

import simclient.cli.utilities.CommandParser;
import simcommon.identification.IHostnameObtainedCallback;
import simcommon.identification.IHostnameObtainer;
import simcommon.utilities.ParamUtil;

public class GuiHostnameObtainer implements IHostnameObtainer {
	
	String userInputStr;
	
	public GuiHostnameObtainer(String userInput){
		userInputStr = userInput;
		System.out.println("User input is:" + userInputStr);
	}

	public void obtainHostname(IHostnameObtainedCallback callback) {

		 do{
			 userInputStr = JOptionPane.showInputDialog("Enter Server Hostname:","");
		 }while(userInputStr.equals("")); //returns null if canceled
		 if (userInputStr != null){
			 callback.onHostnameObtained(userInputStr);
		 }
	}
    
}




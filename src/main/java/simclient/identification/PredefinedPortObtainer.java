package simclient.identification;

import simcommon.Constants;
import simcommon.identification.IPortObtainedCallback;
import simcommon.identification.IPortObtainer;

public class PredefinedPortObtainer implements IPortObtainer {

	private int mPort;
	
	public PredefinedPortObtainer(){
		mPort = Constants.SIM_SERVER_PORT_NUMBER;
	}
	
	public void obtainPort(IPortObtainedCallback callback) {
		callback.onPortObtained(mPort);
	}
}

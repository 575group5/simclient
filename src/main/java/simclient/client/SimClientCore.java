package simclient.client;

import java.io.IOException;

import simclient.authentication.NameOnlyAuthenticationObtainer;
import simcommon.identification.HostnamePortIdentity;
import simcommon.identification.IServerIdentity;
import simcommon.serialization.DelegatingMessageDeserializer;
import simcommon.serialization.DelegatingMessageSerializer;
import simcommon.serialization.IMessageDeserializer;
import simcommon.serialization.IMessageSerializer;
import simcommon.serialization.ISpecificMessageSerializerDeserializerFactory;
import simcommon.serialization.simple.factories.SimpleAuthenticateRequestMessageSerializationFactory;
import simcommon.serialization.simple.factories.SimpleAuthenticateResponseMessageSerializationFactory;
import simcommon.serialization.simple.factories.SimpleClientSendMessageMessageSerializationFactory;
import simcommon.serialization.simple.factories.SimpleDirectoryRequestMessageSerializationFactory;
import simcommon.serialization.simple.factories.SimpleDirectoryResponseMessageSerializationFactory;
import simcommon.serialization.simple.factories.SimpleDisconnectMessageSerializationFactory;
import simcommon.serialization.simple.factories.SimpleServerFailureMessageSerializationFactory;
import simcommon.serialization.simple.factories.SimpleServerSendMessageMessageSerializationFactory;
import simcommon.serialization.simple.factories.SimpleServerSuccessMessageSerializationFactory;
import simcommon.systemmessagecentral.SocketBasedSystemMessageCentral;
import simcommon.systemmessagecentral.SystemMessageCentral;

public class SimClientCore extends ClientCore {

	private IMessageSerializer mSerializer;
	private IMessageDeserializer mDeserializer;

	public SimClientCore(simclient.authentication.HostnamePortIdentityObtainer hostObtainer,
			NameOnlyAuthenticationObtainer authObtainer) {
		super(hostObtainer, authObtainer);
		setUpSerializerAndDeserializer();
	}

	@Override
	public SystemMessageCentral createSystemMessageCentral(IServerIdentity identity)
			throws SystemMessageCentralCreationException {
		// we know we'll get a HostnamePortIdentity, because our
		// IdentityObtainer is a HostnamePortIdentityObtainer (see constructor
		// above)
		try {
			return new SocketBasedSystemMessageCentral(mSerializer, mDeserializer, (HostnamePortIdentity) identity);
		} catch (IOException e) {
			throw new SystemMessageCentralCreationException("problem creating with given hostname and port", e);
		}
	}

	private void setUpSerializerAndDeserializer() {
		DelegatingMessageDeserializer.Builder deserializerBuilder = new DelegatingMessageDeserializer.Builder();
		DelegatingMessageSerializer.Builder serializerBuilder = new DelegatingMessageSerializer.Builder();
		addSerializerAndDeserializerFromFactory(deserializerBuilder, serializerBuilder,
				new SimpleAuthenticateRequestMessageSerializationFactory());
		addSerializerAndDeserializerFromFactory(deserializerBuilder, serializerBuilder,
				new SimpleAuthenticateResponseMessageSerializationFactory());
		addSerializerAndDeserializerFromFactory(deserializerBuilder, serializerBuilder,
				new SimpleDirectoryRequestMessageSerializationFactory());
		addSerializerAndDeserializerFromFactory(deserializerBuilder, serializerBuilder,
				new SimpleDirectoryResponseMessageSerializationFactory());
		addSerializerAndDeserializerFromFactory(deserializerBuilder, serializerBuilder,
				new SimpleClientSendMessageMessageSerializationFactory());
		addSerializerAndDeserializerFromFactory(deserializerBuilder, serializerBuilder,
				new SimpleServerSendMessageMessageSerializationFactory());
		addSerializerAndDeserializerFromFactory(deserializerBuilder, serializerBuilder,
				new SimpleServerFailureMessageSerializationFactory());
		addSerializerAndDeserializerFromFactory(deserializerBuilder, serializerBuilder,
				new SimpleServerSuccessMessageSerializationFactory());
		addSerializerAndDeserializerFromFactory(deserializerBuilder, serializerBuilder,
				new SimpleDisconnectMessageSerializationFactory());
		mDeserializer = deserializerBuilder.build();
		mSerializer = serializerBuilder.build();
	}

	private void addSerializerAndDeserializerFromFactory(DelegatingMessageDeserializer.Builder deserializerBuilder,
			DelegatingMessageSerializer.Builder serializerBuilder,
			ISpecificMessageSerializerDeserializerFactory factory) {
		deserializerBuilder.add(factory.getSpecificDeserializer());
		serializerBuilder.add(factory.getSpecificSerializer());
	}

}

package simclient.client.listeners;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import simcommon.conversation.Conversation;
import simcommon.messages.userMessages.IUserMessage;
import simcommon.messages.userMessages.origin.IUserMessageOrigin;
import simcommon.utilities.ParamUtil;

public class MessageReceivedDelegatingListener implements IUserMessageReceivedListener {

	private Collection<SpecificUserMessageReceivedListener> mMessageReceivedListeners;
	
	private MessageReceivedDelegatingListener(Builder builder) {
		mMessageReceivedListeners = Collections.unmodifiableCollection(new ArrayList<>(builder.mMessageReceivedListeners));
	}
	
	@Override
	public void onMessageReceived(IUserMessageOrigin source, Conversation conversation, IUserMessage message) {
		ParamUtil.validateParamNotNull("message source cannot be null", source);
		ParamUtil.validateParamNotNull("message cannot be null", message);
		for (SpecificUserMessageReceivedListener listener : mMessageReceivedListeners) {
			if (listener.canHandle(message)) {
				listener.onMessageReceived(source, conversation, message);
			}
		}
	}

	public static class Builder{
		private Collection<SpecificUserMessageReceivedListener> mMessageReceivedListeners = new ArrayList<>();

		public Builder add(SpecificUserMessageReceivedListener listener) {
			ParamUtil.validateParamNotNull("listener cannot be null", listener);
			mMessageReceivedListeners.add(listener);
			return this;
		}

		public MessageReceivedDelegatingListener build() {
			return new MessageReceivedDelegatingListener(this);
		}
	}

}
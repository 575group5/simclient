package simclient.client.listeners;

public interface IAuthenticationStatusListener {

	void onAuthenticationCheckBegin();

	void onAuthenticationSuccess();

	void onAuthenticationFailure();
}

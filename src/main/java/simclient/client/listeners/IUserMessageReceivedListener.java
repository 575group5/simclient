package simclient.client.listeners;

import simcommon.conversation.Conversation;
import simcommon.messages.userMessages.IUserMessage;
import simcommon.messages.userMessages.origin.IUserMessageOrigin;

public interface IUserMessageReceivedListener {
	
	void onMessageReceived(IUserMessageOrigin source, Conversation conversation, IUserMessage message);
}

package simclient.client.listeners;

import simcommon.conversation.Conversation;
import simcommon.messages.userMessages.IUserMessage;

public interface ISentMessageStatusListener {

	void onMessageSendSuccessful(Conversation conversation, IUserMessage message);

	void onMessageSendFail(Conversation conversation, IUserMessage message);
}

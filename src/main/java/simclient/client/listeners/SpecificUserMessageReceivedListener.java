package simclient.client.listeners;

import simcommon.conversation.Conversation;
import simcommon.conversation.IConversationIdentifier;
import simcommon.messages.userMessages.IUserMessage;
import simcommon.messages.userMessages.origin.IUserMessageOrigin;
import simcommon.messages.userMessages.origin.IUserMessageOriginIdentifier;
import simcommon.utilities.ParamUtil;

public abstract class SpecificUserMessageReceivedListener {
	protected IConversationIdentifier mUserMessageConversationIdentifier;
	protected IUserMessageOriginIdentifier mUserMessageOriginIdentifier;
	
	protected SpecificUserMessageReceivedListener(IConversationIdentifier conversationIdentifier, 
			IUserMessageOriginIdentifier originIdentifier){
		ParamUtil.validateParamNotNull("Conversation Identifier must be supplied.", conversationIdentifier);
		ParamUtil.validateParamNotNull("User Message Origin Identifier must be supplied.", originIdentifier);
		mUserMessageConversationIdentifier = conversationIdentifier;
		mUserMessageOriginIdentifier = originIdentifier;
	}
	public abstract void onMessageReceived(IUserMessageOrigin source, Conversation conversation, IUserMessage message);
	public abstract boolean canHandle(IUserMessage message);
}

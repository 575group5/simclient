package simclient.client.listeners;

import java.util.Collection;

import simcommon.conversation.Conversation;

public interface IConversationListReceivedListener {

	void onConversationListReceived(Collection<Conversation> conversations);
}

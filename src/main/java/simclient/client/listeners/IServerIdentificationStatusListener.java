package simclient.client.listeners;

public interface IServerIdentificationStatusListener {

	void onServerIdentificationSuccess();

	void onServerIdentificationFailure();
}

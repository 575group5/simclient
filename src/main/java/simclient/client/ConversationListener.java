package simclient.client;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.CopyOnWriteArrayList;

import simclient.client.listeners.IConversationListReceivedListener;
import simcommon.conversation.Conversation;
import simcommon.entities.User;
import simcommon.messages.systemMessages.DirectoryRequestSystemMessage;
import simcommon.messages.systemMessages.DirectoryResponseSystemMessage;
import simcommon.messages.systemMessages.SystemMessage;
import simcommon.systemmessagecentral.IStatusCallback;
import simcommon.systemmessagecentral.ISystemMessageReceivedListener;
import simcommon.systemmessagecentral.SystemMessageCentral;

public class ConversationListener implements ISystemMessageReceivedListener {

	protected Collection<IConversationListReceivedListener> mListeners = new CopyOnWriteArrayList<>();
	
	private Collection<Conversation> mAvailableConversations = new ArrayList<Conversation>();
	
	public void requestConversationList(SystemMessageCentral systemMessageCentral, User user) {
		systemMessageCentral.sendMessage(new DirectoryRequestSystemMessage(user), new IStatusCallback() {

			@Override
			public void onSuccess() {
			}

			@Override
			public void onFailure() {
				mAvailableConversations.clear();
			}
		});
	}
	
	public void attachConversationListReceivedListener(IConversationListReceivedListener listener) {
		mListeners.add(listener);
	}
	
	private void notifyConversationListReceived(DirectoryResponseSystemMessage message) {
		for(IConversationListReceivedListener listener : mListeners){
			listener.onConversationListReceived(message.getConversations());
		}
	}

	public Collection<Conversation> getConversations() {
		return Collections.unmodifiableCollection(mAvailableConversations);
	}

	@Override
	public void onMessageReceived(SystemMessage message) {
		if (message instanceof DirectoryResponseSystemMessage) {
			DirectoryResponseSystemMessage directoryResponseSystemMessage = (DirectoryResponseSystemMessage) message;
			Collection<Conversation> conversations = directoryResponseSystemMessage.getConversations();
			mAvailableConversations.clear();
			for (Conversation conversation : conversations) {
				mAvailableConversations.add(conversation);
			}
			notifyConversationListReceived(directoryResponseSystemMessage);
		}
	}
}

package simclient.client;

import simclient.client.listeners.ISentMessageStatusListener;
import simcommon.conversation.Conversation;
import simcommon.entities.User;
import simcommon.messages.systemMessages.ClientSendMessageRequestSystemMessage;
import simcommon.messages.systemMessages.GeneralServerFailureResponseSystemMessage;
import simcommon.messages.systemMessages.GeneralServerSuccessResponseSystemMessage;
import simcommon.messages.systemMessages.SystemMessage;
import simcommon.messages.userMessages.IUserMessage;
import simcommon.systemmessagecentral.IStatusCallback;
import simcommon.systemmessagecentral.ISystemMessageReceivedListener;
import simcommon.systemmessagecentral.SystemMessageCentral;

public class MessageSender implements ISystemMessageReceivedListener {

	private boolean mWaitingForStatus = false;
	private ISentMessageStatusListener mStoredListener;
	private Conversation mStoredConversation;
	private IUserMessage mStoredMessage;
	
	public void sendMessage(SystemMessageCentral systemMessageCentral, User user, Conversation conversation,
			IUserMessage message, ISentMessageStatusListener callback) {
		systemMessageCentral.sendMessage(new ClientSendMessageRequestSystemMessage(user, conversation, message),
				new IStatusCallback() {

					@Override
					public void onSuccess() {
						mWaitingForStatus = true;
						mStoredListener = callback;
						mStoredConversation = conversation;
						mStoredMessage = message;
					}

					@Override
					public void onFailure() {
						callback.onMessageSendFail(conversation, message);
					}
				});
	}

	@Override
	public void onMessageReceived(SystemMessage message) {
		if (mWaitingForStatus) {
			if (message instanceof GeneralServerSuccessResponseSystemMessage) {
				mStoredListener.onMessageSendSuccessful(mStoredConversation, mStoredMessage);
				clearStoredData();
			} else if (message instanceof GeneralServerFailureResponseSystemMessage) {
				mStoredListener.onMessageSendFail(mStoredConversation, mStoredMessage);
				clearStoredData();
			}
		}
	}
	
	private void clearStoredData() {
		mWaitingForStatus = false;
		mStoredListener = null;
		mStoredConversation = null;
		mStoredMessage = null;
	}
}

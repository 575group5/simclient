package simclient.client;

import simclient.cli.CliClientLauncher;

public class UserInterfaceLauncher {

	public static void main(String[] args){
		CliClientLauncher userInterface = new CliClientLauncher();
		userInterface.launchInterface();
	}
}
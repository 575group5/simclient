package simclient.client;

import simcommon.entities.User;
import simcommon.messages.systemMessages.ClientDisconnectRequestSystemMessage;
import simcommon.systemmessagecentral.SystemMessageCentral;

public class Disconnector {

	public void disconnectUser(SystemMessageCentral systemMessageCentral, User user) {
		systemMessageCentral.sendMessage(new ClientDisconnectRequestSystemMessage(user), null);
	}

}

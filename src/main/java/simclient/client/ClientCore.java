package simclient.client;

import java.util.Collection;

import simclient.authentication.IAuthenticationObtainer;
import simclient.client.listeners.IAuthenticationStatusListener;
import simclient.client.listeners.IConversationListReceivedListener;
import simclient.client.listeners.ISentMessageStatusListener;
import simclient.client.listeners.IServerIdentificationStatusListener;
import simclient.client.listeners.IUserMessageReceivedListener;
import simcommon.conversation.Conversation;
import simcommon.identification.IServerIdentityObtainer;
import simcommon.messages.userMessages.IUserMessage;
import simcommon.systemmessagecentral.SystemMessageCentral;
import simcommon.utilities.ParamUtil;

public abstract class ClientCore implements ISystemMessageCentralCreator {

	private ConversationListener mConversationLister;
	private Authenticator mAuthenticator;
	private Disconnector mDisconnector;
	private MessageSender mMessageSender;
	private MessageReceiver mMessageReceiver;

	public ClientCore(IServerIdentityObtainer serverIdentityObtainer, IAuthenticationObtainer authObtainer) {
		ParamUtil.validateParamNotNull("must have server identiy obtainer", serverIdentityObtainer);
		ParamUtil.validateParamNotNull("must have authentication obtainer", authObtainer);

		initializeConversationRequester();
		mAuthenticator = new Authenticator(this, serverIdentityObtainer, authObtainer);
		mDisconnector = new Disconnector();
		mMessageSender = new MessageSender();
		mMessageReceiver = new MessageReceiver();
	}

	public void start() {
		mAuthenticator.attachAuthenticationStatusListener(new IAuthenticationStatusListener() {

			@Override
			public void onAuthenticationSuccess() {
				SystemMessageCentral systemMessageCentral = mAuthenticator.getMessageCentralForUser();
				systemMessageCentral.attachSystemMessageReceivedListener(mConversationLister);
				systemMessageCentral.attachSystemMessageReceivedListener(mMessageSender);
				systemMessageCentral.attachSystemMessageReceivedListener(mMessageReceiver);
			}

			@Override
			public void onAuthenticationFailure() {
			}

			@Override
			public void onAuthenticationCheckBegin() {
			}
		});
		mAuthenticator.startAuthorizationProcess();
	}

	public void stop() {
		if (mAuthenticator.isAuthenticated()) {
			mDisconnector.disconnectUser(mAuthenticator.getMessageCentralForUser(),
					mAuthenticator.getAuthenticatedUser());
			mAuthenticator.reset();
		}
	}
	
	public void requestConversationList() {
		if (!mAuthenticator.isAuthenticated()) {
			throw new IllegalStateException("cannot request conversation list before user authenticated");
		}
		mConversationLister.requestConversationList(mAuthenticator.getMessageCentralForUser(),
				mAuthenticator.getAuthenticatedUser());
	}

	public void attachConversationListReceivedListener(IConversationListReceivedListener listener) {
		mConversationLister.attachConversationListReceivedListener(listener);
	}

	public void attachMessageReceivedListener(IUserMessageReceivedListener listener) {
		mMessageReceiver.attachMessageReceivedListener(listener);
	}

	public void attachServerIdentificationStatusListener(IServerIdentificationStatusListener listener) {
		mAuthenticator.attachServerIdentificationStatusListener(listener);
	}

	public void attachAuthenticationStatusListener(IAuthenticationStatusListener listener) {
		mAuthenticator.attachAuthenticationStatusListener(listener);
	}

	public Collection<Conversation> getConversations() {
		return mConversationLister.getConversations();
	}
	
	public void sendMessage(Conversation conversation, IUserMessage message, ISentMessageStatusListener callback) {
		if (!mAuthenticator.isAuthenticated()) {
			throw new IllegalStateException("cannot send message before we connect and authenticate");
		}
		mMessageSender.sendMessage(mAuthenticator.getMessageCentralForUser(), mAuthenticator.getAuthenticatedUser(),
				conversation, message, callback);
	}

	private void initializeConversationRequester(){
		mConversationLister = new ConversationListener();
	}

}

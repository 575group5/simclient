package simclient.client;

import simcommon.identification.IServerIdentity;
import simcommon.systemmessagecentral.SystemMessageCentral;

public interface ISystemMessageCentralCreator {
	SystemMessageCentral createSystemMessageCentral(IServerIdentity identity)
			throws SystemMessageCentralCreationException;

	public class SystemMessageCentralCreationException extends Exception {
		private static final long serialVersionUID = -2490027894711467655L;

		public SystemMessageCentralCreationException(String message, Exception e) {
			super(message, e);
		}
	}
}
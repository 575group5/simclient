package simclient.client;

import java.util.Collection;
import java.util.concurrent.CopyOnWriteArrayList;

import simclient.authentication.IAuthenticationObtainedCallback;
import simclient.authentication.IAuthenticationObtainer;
import simclient.client.ISystemMessageCentralCreator.SystemMessageCentralCreationException;
import simclient.client.listeners.IAuthenticationStatusListener;
import simclient.client.listeners.IServerIdentificationStatusListener;
import simcommon.authentication.IUserAuthentication;
import simcommon.entities.User;
import simcommon.identification.IServerIdentity;
import simcommon.identification.IServerIdentityObtainedCallback;
import simcommon.identification.IServerIdentityObtainer;
import simcommon.messages.systemMessages.AuthenticationRequestSystemMessage;
import simcommon.messages.systemMessages.AuthenticationResponseSystemMessage;
import simcommon.messages.systemMessages.SystemMessage;
import simcommon.systemmessagecentral.IStatusCallback;
import simcommon.systemmessagecentral.ISystemMessageReceivedListener;
import simcommon.systemmessagecentral.SystemMessageCentral;
import simcommon.utilities.ParamUtil;

public class Authenticator implements ISystemMessageReceivedListener {
	// for notifying clients when the user is authenticated or authentication
	// fails
	private Collection<IAuthenticationStatusListener> mAuthenticationListeners = new CopyOnWriteArrayList<>();
	// for notifying clients when we've successfully connected to the server, or
	// when connecting failed
	private Collection<IServerIdentificationStatusListener> mServerIdStatusListeners = new CopyOnWriteArrayList<>();

	private ISystemMessageCentralCreator mSystemMessageCentralCreator;
	private SystemMessageCentral mMessageCentral;

	private User mUser;

	// we obtain these together. if there's an error with one or the other,
	// store so we don't re-prompt
	private IServerIdentity mStoredIdentity;
	private IUserAuthentication mStoredAuthentication;

	private IServerIdentityObtainer mServerIdentityObtainer;
	private IAuthenticationObtainer mAuthenticationObtainer;

	public Authenticator(ISystemMessageCentralCreator systemMessageCentralCreator,
			IServerIdentityObtainer serverIdentityObtainer, IAuthenticationObtainer authObtainer) {
		ParamUtil.validateParamNotNull("must have server identiy obtainer", serverIdentityObtainer);
		ParamUtil.validateParamNotNull("must have authentication obtainer", authObtainer);
		mSystemMessageCentralCreator = systemMessageCentralCreator;
		mServerIdentityObtainer = serverIdentityObtainer;
		mAuthenticationObtainer = authObtainer;
	}

	public void attachAuthenticationStatusListener(IAuthenticationStatusListener listener) {
		if (listener != null && !mAuthenticationListeners.contains(listener)) {
			mAuthenticationListeners.add(listener);
		}
	}

	public void attachServerIdentificationStatusListener(IServerIdentificationStatusListener listener) {
		mServerIdStatusListeners.add(listener);
	}

	public boolean isAuthenticated() {
		return mUser != null;
	}

	public User getAuthenticatedUser() {
		return mUser;
	}

	public SystemMessageCentral getMessageCentralForUser() {
		return mMessageCentral;
	}

	public void startAuthorizationProcess() {
		obtainServerIdentity();
	}

	public void reset() {
		mMessageCentral = null;
		mUser = null;
	}

	private void obtainServerIdentity() {
		if (mStoredIdentity == null) {
			mServerIdentityObtainer.obtainServerIdentity(new IServerIdentityObtainedCallback() {
				@Override
				public void onServerIdentityObtained(IServerIdentity identity) {
					mStoredIdentity = identity;
					obtainAuthentication();
				}
			});
		} else {
			obtainAuthentication();
		}
	}

	private void obtainAuthentication() {
		if (mStoredAuthentication == null) {
			mAuthenticationObtainer.obtainAuthentication(new IAuthenticationObtainedCallback() {

				@Override
				public void onAuthenticationObtained(IUserAuthentication authMechanism) {
					mStoredAuthentication = authMechanism;
					connectToServer(mStoredIdentity, mStoredAuthentication);
				}
			});
		} else {
			connectToServer(mStoredIdentity, mStoredAuthentication);
		}
	}

	private void connectToServer(IServerIdentity identity, IUserAuthentication authMechanism) {
		if (mMessageCentral == null) {
			// we haven't successfully constructed it yet. try now
			try {
				mMessageCentral = mSystemMessageCentralCreator.createSystemMessageCentral(identity);
				mMessageCentral.attachSystemMessageReceivedListener(this);
			} catch (SystemMessageCentralCreationException e) {
				notifyServerConnectFailure();
				mStoredIdentity = null;
				obtainServerIdentity();
			}
		}
		if (mMessageCentral != null) {
			notifyUserAuthenticationBegin();
			mMessageCentral.sendMessage(new AuthenticationRequestSystemMessage(authMechanism), new IStatusCallback() {

				@Override
				public void onSuccess() {
					notifyServerConnectSuccess();
				}

				@Override
				public void onFailure() {
					notifyServerConnectFailure();
					mStoredIdentity = null;
					obtainServerIdentity();
				}
			});
			mMessageCentral.startListeningForMessages();
		}
	}

	@Override
	public void onMessageReceived(SystemMessage message) {
		if (mUser == null) {
			// we're not authenticated. only message we want to receive is a
			// authentication response
			if (message instanceof AuthenticationResponseSystemMessage) {
				AuthenticationResponseSystemMessage authenticationResponse = (AuthenticationResponseSystemMessage) message;
				if (authenticationResponse.authenticationSuccessful()) {
					mUser = authenticationResponse.getUser();
					notifyUserAuthenticationSuccess();
				} else {
					mStoredAuthentication = null;
					notifyUserAuthenticationFailure();
					obtainAuthentication();
				}
			} else {
				// received some other message we aren't expecting
				// server shouldn't send us stuff we don't expect. print a
				// warning?
			}
		}
	}

	private void notifyUserAuthenticationBegin() {
		for (IAuthenticationStatusListener listener : mAuthenticationListeners) {
			listener.onAuthenticationCheckBegin();
		}
	}

	private void notifyUserAuthenticationSuccess() {
		for (IAuthenticationStatusListener listener : mAuthenticationListeners) {
			listener.onAuthenticationSuccess();
		}
	}

	private void notifyUserAuthenticationFailure() {
		for (IAuthenticationStatusListener listener : mAuthenticationListeners) {
			listener.onAuthenticationFailure();
		}
	}

	private void notifyServerConnectFailure() {
		for (IServerIdentificationStatusListener listener : mServerIdStatusListeners) {
			listener.onServerIdentificationFailure();
		}
	}

	private void notifyServerConnectSuccess() {
		for (IServerIdentificationStatusListener listener : mServerIdStatusListeners) {
			listener.onServerIdentificationSuccess();
		}
	}
}

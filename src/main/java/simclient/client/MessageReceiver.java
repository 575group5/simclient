package simclient.client;

import java.util.Collection;
import java.util.concurrent.CopyOnWriteArrayList;

import simclient.client.listeners.IUserMessageReceivedListener;
import simcommon.messages.systemMessages.ServerSendMessageRequestSystemMessage;
import simcommon.messages.systemMessages.SystemMessage;
import simcommon.systemmessagecentral.ISystemMessageReceivedListener;

public class MessageReceiver implements ISystemMessageReceivedListener {

	// for notifying clients when a user message is received from another user
	private Collection<IUserMessageReceivedListener> mMessageReceivedListeners = new CopyOnWriteArrayList<>();

	public void attachMessageReceivedListener(IUserMessageReceivedListener listener) {
		mMessageReceivedListeners.add(listener);
	}

	@Override
	public void onMessageReceived(SystemMessage message) {
		if (message instanceof ServerSendMessageRequestSystemMessage) {
			notifyClientSendRequestMessageReceived((ServerSendMessageRequestSystemMessage) message);
		}
	}

	public void notifyClientSendRequestMessageReceived(ServerSendMessageRequestSystemMessage message) {
		for (IUserMessageReceivedListener listener : mMessageReceivedListeners) {
			listener.onMessageReceived(message.getSource(), message.getConversation(), message.getMessage());
		}
	}
}

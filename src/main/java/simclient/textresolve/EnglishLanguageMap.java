package simclient.textresolve;

import java.util.HashMap;
import java.util.Map;

public class EnglishLanguageMap extends LanguageMap {
	
	private static final Map<Text, String> ENGLISH_TEXT;
	private static final Map<ErrorMessage, String> ENGLISH_ERROR_MESSAGES;
	static {
		ENGLISH_TEXT = new HashMap<>(Text.values().length);
		ENGLISH_TEXT.put(Text.WELCOME_MESSAGE, getWelcome());
		ENGLISH_TEXT.put(Text.ENTER_COMMANDS_MESSAGE, "You may now enter commands.");
		ENGLISH_TEXT.put(Text.HOW_TO_QUIT_MESSAGE, "To exit application, enter:");
		ENGLISH_TEXT.put(Text.CONNECTED_MESSAGE, "You are now connected to the server.");
		ENGLISH_TEXT.put(Text.DISCONNECTED_MESSAGE, "You are now disconnected from the server.");
		ENGLISH_TEXT.put(Text.GOODBYE, "SiMessenger is now closing. Goodbye.");
	}
	
	static{
		ENGLISH_ERROR_MESSAGES = new HashMap<>(ErrorMessage.values().length);
		ENGLISH_ERROR_MESSAGES.put(ErrorMessage.ERR_SERVER_ADDRESS, "The supplied server address is malformed.");
		ENGLISH_ERROR_MESSAGES.put(ErrorMessage.ERR_SOCKET_OPEN, "A valid SiMessenger server cannot be found at the supplied address.");
		ENGLISH_ERROR_MESSAGES.put(ErrorMessage.ERR_NOT_CONNECTED, "You are disconnected from the SiMessenger server. The command is not allowed.");
	}

	protected Map<Text, String> getTextMap() {
		return ENGLISH_TEXT;
	}

	protected Map<ErrorMessage, String> getErrorMessageMap() {
		return ENGLISH_ERROR_MESSAGES;
	}
	
	static String getWelcome(){
		StringBuilder builder = new StringBuilder();
		builder.append("Welcome to\n");
		builder.append("=============================================================\n");
		builder.append("  _____ _ __  __\n");
		builder.append(" / ____(_)  \\/  |\n");
		builder.append("| (___  _| \\  / | ___  ___ ___  ___ _ __   __ _  ___ _ __\n");
		builder.append(" \\___ \\| | |\\/| |/ _ \\/ __/ __|/ _ \\ '_ \\ / _` |/ _ \\ '__|\n");
		builder.append(" ____) | | |  | |  __/\\__ \\__ \\  __/ | | | (_| |  __/ |\n");
		builder.append("|_____/|_|_|  |_|\\___||___/___/\\___|_| |_|\\__, |\\___|_|\n");
		builder.append("                                           __/ |\n");
		builder.append("                                          |___/    \n");
		builder.append("=============================================================\n");
		return builder.toString();
	}

}

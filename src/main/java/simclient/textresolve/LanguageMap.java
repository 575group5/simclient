package simclient.textresolve;

import java.util.Map;

public abstract class LanguageMap implements TextResolver {
	protected abstract Map<Text, String> getTextMap();
	protected abstract Map<ErrorMessage, String> getErrorMessageMap();
	
	public String getStringForText(Text text) {
		return getTextMap().get(text);
	}

	public String getStringForErrorMessage(ErrorMessage error){
		return getErrorMessageMap().get(error);
	}
}

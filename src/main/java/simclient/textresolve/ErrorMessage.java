package simclient.textresolve;

public enum ErrorMessage {
	ERR_SERVER_ADDRESS,
	ERR_SOCKET_OPEN,
	ERR_NOT_CONNECTED
}

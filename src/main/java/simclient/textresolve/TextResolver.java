package simclient.textresolve;

public interface TextResolver {
	String getStringForText(Text text);
}

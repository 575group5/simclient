package simclient.message;

import simcommon.messages.userMessages.origin.IUserMessageOrigin;
import simcommon.messages.userMessages.origin.IUserMessageOriginIdentifier;
import simcommon.messages.userMessages.origin.ServerOrigin;
import simcommon.messages.userMessages.origin.UserOrigin;

public class SingleUserMessageOriginIdentifier implements IUserMessageOriginIdentifier {

	@Override
	public String getOriginIdentifier(IUserMessageOrigin origin) {
		if(origin instanceof UserOrigin){
			return ((UserOrigin) origin).getOriginUser().getUserInfo().getName();
		} else if (origin instanceof ServerOrigin) {
			return "Server";
		}
		return "Unknown";
	}

}

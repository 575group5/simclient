package simclient.gui;
//package test;

import java.awt.Color;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.Component;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.JList;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;
import java.awt.event.ActionEvent;

import simclient.authentication.HostnamePortIdentityObtainer;
import simclient.cli.authentication.GuiNameOnlyAuthenticationObtainer;
import simclient.client.ClientCore;
import simclient.client.SimClientCore;
import simclient.client.listeners.IAuthenticationStatusListener;
import simclient.client.listeners.IConversationListReceivedListener;
import simclient.client.listeners.ISentMessageStatusListener;
import simclient.client.listeners.IServerIdentificationStatusListener;
import simclient.client.listeners.IUserMessageReceivedListener;
import simclient.conversation.IConversationAssociater;
import simclient.conversation.IncrementalIntegerConversationAssociater;
import simclient.identification.GuiHostnameObtainer;
import simclient.identification.PredefinedPortObtainer;
import simcommon.conversation.Conversation;
import simcommon.messages.userMessages.IUserMessage;
import simcommon.messages.userMessages.PlainAsciiUserMessage;
import simcommon.messages.userMessages.origin.IUserMessageOrigin;
import javax.swing.JTabbedPane;
import javax.swing.JComponent;


public class GuiClientLauncher extends JFrame {

	private static final long serialVersionUID = 1780063014501294721L;
	private JPanel contentPane;
	private JTextField serverField;
	private JTextField userField;
	private JTextArea userChatInput;
	private JButton buttonConnect;
	private JButton buttonSend ;
	private JButton buttonGetDirectory;
	private JTextArea chatArea;
	private ClientCore mClientCore;
	private JList<Map.Entry<String, Conversation>> mConversationList;
	private IConversationAssociater mConversationAssociater;
    private String userName;
    private String serverName;
	private JPanel[] panel = new JPanel[11];
	private JTextArea[] chatPanes = new JTextArea[11];
	
private JTabbedPane tabbedPane;

	// Launch the application.
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GuiClientLauncher frame = new GuiClientLauncher();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	// Create the frame.
	public GuiClientLauncher() {
		
		mConversationAssociater = new IncrementalIntegerConversationAssociater();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//create panels labels and fields
		setBounds(100, 100, 525, 549);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel label = new JLabel("Server");
		label.setBounds(6, 11, 61, 16);
	
		serverField = new JTextField();
		serverField.setColumns(10);
		serverField.setBounds(46, 6, 130, 26);
		
		JLabel label_1 = new JLabel("User");
		label_1.setBounds(186, 11, 29, 16);

		userField = new JTextField();
		userField.setColumns(10);
		userField.setBounds(222, 6, 126, 26);

		buttonConnect = new JButton("Login");
		
		//setup listener for button click
		buttonConnect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {        
				GuiHostnameObtainer mHostnameObtainer = new GuiHostnameObtainer(serverName);
				PredefinedPortObtainer mPortObtainer = new PredefinedPortObtainer();
				HostnamePortIdentityObtainer hostObtainer = new HostnamePortIdentityObtainer(mHostnameObtainer, mPortObtainer);
				GuiNameOnlyAuthenticationObtainer authObtainer = new GuiNameOnlyAuthenticationObtainer(userName);
				mClientCore = new SimClientCore(hostObtainer, authObtainer);
				attachCoreListeners();
				mClientCore.start();
			}
		});
		
		buttonConnect.setBounds(356, 497, 143, 24);
		contentPane.add(buttonConnect);
		
		buttonSend = new JButton("Send");
		//setup send button click action handler
		buttonSend.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(mConversationList.isSelectionEmpty()){
					JOptionPane.showMessageDialog(contentPane, "You must first select a conversation before sending a message.");
					return;
				}
				
				Conversation targetConversation = mConversationList.getSelectedValue().getValue();
				mClientCore.sendMessage(targetConversation, new PlainAsciiUserMessage(userChatInput.getText()), new ISentMessageStatusListener() {
					@Override
					public void onMessageSendSuccessful(Conversation conversation, IUserMessage message) {
						PlainAsciiUserMessage asciiMessage = (PlainAsciiUserMessage)message;
						mConversationList.getMinSelectionIndex();
						chatPanes[mConversationList.getMinSelectionIndex()+1].append("You said: " + asciiMessage.getContent() + "\n");
					}
					
					@Override
					public void onMessageSendFail(Conversation conversation, IUserMessage message) {
						PlainAsciiUserMessage asciiMessage = (PlainAsciiUserMessage)message;
						chatArea.append("Your follow message failed to send: '" + asciiMessage.getContent() + "'");
					}
				});
			}
		});
		buttonSend.setBounds(356, 467, 143, 29);
		contentPane.add(buttonSend);
		
		userChatInput = new JTextArea();
		userChatInput.setText("Type Message Here");
		
		userChatInput.setBounds(16, 435, 317, 86);
		userChatInput.setLineWrap(true);
		userChatInput.setAutoscrolls(true);
		contentPane.add(userChatInput);
		
		chatArea = new JTextArea();
		chatArea.setBounds(378, 118, 61, 41);
		chatArea.setLineWrap(true);
		chatArea.setAutoscrolls(true);
		
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(6, 45, 342, 385);
		contentPane.add(tabbedPane);

		tabbedPane.addTab("System", chatArea);

		mConversationList = new JList<Map.Entry<String, Conversation>>();
		mConversationList.setToolTipText("User List");
		mConversationList.setBounds(356, 61, 147, 353);
		mConversationList.setSelectionBackground(Color.RED);
		contentPane.add(mConversationList);
		
		buttonGetDirectory = new JButton("Get Conversations");
		buttonGetDirectory.setBounds(356, 437, 143, 29);
		contentPane.add(buttonGetDirectory);
		
		JLabel lblChatArea = new JLabel("Message Tabs");
		lblChatArea.setBounds(16, 17, 97, 16);
		contentPane.add(lblChatArea);
		
		JLabel lblUserList = new JLabel("User List");
		lblUserList.setBounds(365, 17, 61, 16);
		contentPane.add(lblUserList);
		
		buttonGetDirectory.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				mClientCore.requestConversationList();
			}
		});
		
		this.addWindowListener(new WindowAdapter(){
			 public void windowClosing(java.awt.event.WindowEvent windowEvent) {
				 mClientCore.stop();
			 }
		});
		
		enableCommands(false);
	}
	
	private void enableCommands(boolean enable){
		userChatInput.setEnabled(enable);
		buttonSend.setEnabled(enable);
		buttonGetDirectory.setEnabled(enable);
		mConversationList.setEnabled(enable);
		serverField.setEnabled(!enable);
		userField.setEnabled(!enable);
		
		if(enable)
			userChatInput.requestFocus();
	}
	
	private void attachCoreListeners() {
		mClientCore.attachAuthenticationStatusListener(new IAuthenticationStatusListener() {
			@Override
			public void onAuthenticationCheckBegin() {
				enableCommands(false);
				chatArea.append("Authenticating...\n");
			}

			@Override
			public void onAuthenticationSuccess() {
				enableCommands(true);
				chatArea.append("Authentication successful!\n");
			}

			@Override
			public void onAuthenticationFailure() {
				chatArea.append("Authentication failed!\n");
			}
		});
		mClientCore.attachConversationListReceivedListener(new IConversationListReceivedListener() {
			@Override
			public void onConversationListReceived(Collection<Conversation> conversations) {
				mConversationList.clearSelection();
				mConversationList.removeAll();
				Map<String, Conversation> associationMap = mConversationAssociater.associateConversations(conversations);
				int i = 1;
				
				DefaultListModel<Map.Entry<String, Conversation>> model = new DefaultListModel<Map.Entry<String, Conversation>>();
				for(Map.Entry<String, Conversation> conversation : associationMap.entrySet()){
					model.addElement(conversation);
						//build panels and tabs based on users available
						panel[i] = new JPanel();
						chatPanes[i] = new JTextArea();
						panel[i].add(chatPanes[i]);
						chatPanes[i].setBounds(378, 118, 61, 41);
						tabbedPane.addTab(Integer.toString(i), panel[i]);
						
					i++;
					buttonGetDirectory.setEnabled(false);
					
				}

				mConversationList.setModel(model);
				mConversationList.setCellRenderer(new ListCellRenderer<Map.Entry<String, Conversation>>() {
					@Override
					public Component getListCellRendererComponent(JList<? extends Entry<String, Conversation>> list,
							Entry<String, Conversation> value, int index, boolean isSelected, boolean cellHasFocus) {
						JLabel convItem =  new JLabel(value.getKey());
						convItem.setOpaque(true);
						if(isSelected)
							convItem.setBackground(Color.blue);
						else
							convItem.setBackground(Color.white);
						return convItem;
					}
				});
			}
		});
		mClientCore.attachMessageReceivedListener(new IUserMessageReceivedListener() {
			@Override
			public void onMessageReceived(IUserMessageOrigin source,
					Conversation conversation, IUserMessage message) {
				PlainAsciiUserMessage asciiMessage = (PlainAsciiUserMessage)message;
				int panelvalue;
				
				panelvalue = Integer.valueOf(mConversationAssociater.resolveConversationToIdentifier(mClientCore.getConversations(), conversation));
				if (mConversationAssociater.resolveConversationToIdentifier(mClientCore.getConversations(), conversation) != null){
					chatPanes[panelvalue].append("You received: " + asciiMessage.getContent() + "\n");
				}
				else{
					chatArea.append("Error on Panel Receive\n");
				}
			}
		});
		mClientCore.attachServerIdentificationStatusListener(new IServerIdentificationStatusListener() {
			@Override
			public void onServerIdentificationSuccess() {
				chatArea.append("Server identification successful!\n");
			}
			
			@Override
			public void onServerIdentificationFailure() {
				chatArea.append("Server identification failed!\n");
			}
		});
	}
}

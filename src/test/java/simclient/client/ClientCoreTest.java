package simclient.client;

import org.junit.Assert;
import org.junit.Test;

import simclient.authentication.HostnamePortIdentityObtainer;
import simclient.authentication.IUsernameObtainedCallback;
import simclient.authentication.NameOnlyAuthenticationObtainer;
import simclient.identification.PredefinedPortObtainer;
import simcommon.identification.IHostnameObtainedCallback;
import simcommon.identification.IHostnameObtainer;
import simcommon.identification.IPortObtainer;

public class ClientCoreTest {

	ClientCore clientCore;
	
	@Test
	public void TestCreateClientCore(){
		IHostnameObtainer hostObtainer = new IHostnameObtainer() {
			@Override
			public void obtainHostname(IHostnameObtainedCallback callback) {
				callback.onHostnameObtained("localhost");
			}
		};
		NameOnlyAuthenticationObtainer authObtainer = new NameOnlyAuthenticationObtainer() {
			
			@Override
			public void obtainUsername(IUsernameObtainedCallback callback) {
				callback.onUsernameObtained("testUser");
			}
		};
		
		IPortObtainer portObtainer = new PredefinedPortObtainer();
		HostnamePortIdentityObtainer hostPortObtainer = new HostnamePortIdentityObtainer(hostObtainer, portObtainer);
		clientCore = new SimClientCore(hostPortObtainer, authObtainer);
		Assert.assertNotNull(clientCore);
	}
	
}

package simclient.authentication;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import simclient.identification.PredefinedPortObtainer;
import simcommon.Constants;
import simcommon.identification.HostnamePortIdentity;
import simcommon.identification.IHostnameObtainedCallback;
import simcommon.identification.IHostnameObtainer;
import simcommon.identification.IPortObtainer;
import simcommon.identification.IServerIdentity;
import simcommon.identification.IServerIdentityObtainedCallback;

public class HostnamePortIdentityObtainerTest {

	HostnamePortIdentityObtainer hostPortObtainer;
	IServerIdentity hostIdentity;
	
	@Before
	public void TestCreateHostnamePortIdentityObtainer(){
		IHostnameObtainer hostObtainer = new IHostnameObtainer() {
			@Override
			public void obtainHostname(IHostnameObtainedCallback callback) {
				callback.onHostnameObtained("localhost");
			}
		};
		NameOnlyAuthenticationObtainer authObtainer = new NameOnlyAuthenticationObtainer() {
			
			@Override
			public void obtainUsername(IUsernameObtainedCallback callback) {
				callback.onUsernameObtained("testUser");
			}
		};
		
		IPortObtainer portObtainer = new PredefinedPortObtainer();
		hostPortObtainer = new HostnamePortIdentityObtainer(hostObtainer, portObtainer);
		
	}
	
	@Test
	public void TestObtainServerIdentity(){

		hostPortObtainer.obtainServerIdentity(new IServerIdentityObtainedCallback() {
			@Override
			public void onServerIdentityObtained(IServerIdentity identity) {
				hostIdentity = identity;
			}
		});
		Assert.assertEquals("localhost", ((HostnamePortIdentity) hostIdentity).getHostname());
		Assert.assertEquals(Constants.SIM_SERVER_PORT_NUMBER, ((HostnamePortIdentity) hostIdentity).getPort());
	}
}

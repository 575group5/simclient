package simclient.authentication;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.internal.util.reflection.Whitebox;
import org.mockito.runners.MockitoJUnitRunner;

import simcommon.authentication.UsernameAuthentication;

@RunWith(MockitoJUnitRunner.class)
public class UsernameAuthenticationTest {
	@InjectMocks
	UsernameAuthentication testObj;
	
	@Test
	public void testUsername(){
		Whitebox.setInternalState(testObj, "mUsername", "hello");
		assertEquals(testObj.getUsername(),"hello");
	}
}

package simclient.cli.commandresolve;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class EnglishCommandMapTest {

	@InjectMocks
	EnglishCommandMap commandMap;
	
	
	@Test
	public void ResolveExitCommandStringTest(){
		Assert.assertEquals(Command.EXIT, commandMap.resolveStringCommand("exit"));
	}
	
	@Test
	public void ResolveDirectoryCommandStringTest(){
		Assert.assertEquals(Command.DIRECTORY, commandMap.resolveStringCommand("directory"));
	}
	
	@Test
	public void ResolveSendMessageCommandStringTest(){
		Assert.assertEquals(Command.SEND_MESSAGE, commandMap.resolveStringCommand("send"));
	}
	
	@Test
	public void ResolveHelpCommandStringTest(){
		Assert.assertEquals(Command.HELP, commandMap.resolveStringCommand("help"));
	}
	
	@Test
	public void ResolveUnknownCommandStringTest(){
		Assert.assertEquals(Command.UNKNOWN, commandMap.resolveStringCommand("myUnknownCommand"));
	}
}

package simclient.cli;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.internal.util.reflection.Whitebox;
import org.mockito.runners.MockitoJUnitRunner;

import simclient.cli.commandhandler.CommandHandlerSelector;
import simclient.cli.commandresolve.EnglishCommandMap;
import simclient.textresolve.EnglishLanguageMap;
import simclient.textresolve.LanguageMap;

@RunWith(MockitoJUnitRunner.class)
public class CliClientLauncherTest {

	CliClient cliClient;
	
	@Test
	public void getTextGeneratorTest(){
		setupClient();
		Assert.assertNotNull(cliClient.getTextGenerator());
	}
	
	@Test
	public void getCommandInterpreterTest(){
		setupClient();
		Assert.assertNotNull(cliClient.getCommandInterpreter());
	}
	
	private void setupClient(){
		LanguageMap englishMap = new EnglishLanguageMap();
		CommandInterpreter interpreter = new CommandInterpreter(englishMap, new EnglishCommandMap(), new CommandHandlerSelector());
		cliClient = new CliClient(englishMap, interpreter);
	}
}

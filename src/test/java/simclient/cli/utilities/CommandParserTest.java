package simclient.cli.utilities;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class CommandParserTest {

	@Test
	public void ParseEmptyCommandTest(){
		List<String> result = CommandParser.ParseCommand("");
		Assert.assertTrue(result.size() == 0);
	}
	
	@Test
	public void ParseNonEmptyCommandTest(){
		ArrayList<String> expectedResult = new ArrayList<String>();
		expectedResult.add("command");
		expectedResult.add("args 1");
		expectedResult.add("args2");

		List<String> result = CommandParser.ParseCommand("command \"args 1\" args2");
		Assert.assertTrue(result.size() == 3);
		Assert.assertEquals(result.get(0), expectedResult.get(0));
		Assert.assertEquals(result.get(1), expectedResult.get(1));
		Assert.assertEquals(result.get(2), expectedResult.get(2));
	}
}

package simclient.cli.commandhandler;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import simclient.cli.commandresolve.Command;
import simclient.textresolve.EnglishLanguageMap;

@RunWith(MockitoJUnitRunner.class)
public class CommandHandlerSelectorTest {

	@InjectMocks
	CommandHandlerSelector handlerSelector;
	
	@Test
	public void TestAddRetrieveCommandHandler(){
		handlerSelector.addHandler(Command.DIRECTORY, new DirectoryCommandHandler(new EnglishLanguageMap()));
		CommandHandler handler = handlerSelector.getCommandHandler(Command.DIRECTORY);
		Assert.assertTrue(handler instanceof DirectoryCommandHandler);
	}
}

package simclient.cli;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import simclient.cli.commandhandler.CommandHandlerSelector;
import simclient.cli.commandresolve.Command;
import simclient.cli.commandresolve.EnglishCommandMap;
import simclient.textresolve.EnglishLanguageMap;

@RunWith(MockitoJUnitRunner.class)
public class CommandInterpreterTest {

	CommandInterpreter interpreter;
	
	@Before
    public void setUp() {
		interpreter = new CommandInterpreter(new EnglishLanguageMap(), new EnglishCommandMap(), new CommandHandlerSelector());
    }
	
	@Test
	public void TestGetCommandMap(){
		Assert.assertNotNull(interpreter.getCommandMap());
	}
	
	@Test
	public void TestProgramExitRequestedAffirmation(){
		Assert.assertTrue(interpreter.programExitRequested("exit"));
	}
	
	@Test
	public void TestProgramExitRequestedNegation(){
		Assert.assertFalse(interpreter.programExitRequested("dont exit"));
	}
	
	@Test
	public void TestLookupSendCommandString(){
		String connectString = "send";
		Assert.assertEquals(connectString, interpreter.lookupCommandProperties(Command.SEND_MESSAGE).commandName());
	}
	
	@Test
	public void TestLookupUnknownCommandString(){
		String unknownString = null;
		Assert.assertEquals(unknownString, interpreter.lookupCommandProperties(Command.UNKNOWN).commandName());
	}
}

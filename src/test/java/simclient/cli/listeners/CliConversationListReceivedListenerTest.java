package simclient.cli.listeners;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import simclient.conversation.IncrementalIntegerConversationAssociater;
import simcommon.conversation.Conversation;
import simcommon.conversation.ConversationProperties;
import simcommon.conversation.InMemoryConversationIdGenerator;
import simcommon.entities.User;
import simcommon.entities.UserId;
import simcommon.entities.UserInfo;

@RunWith(MockitoJUnitRunner.class)
public class CliConversationListReceivedListenerTest {

	private CliConversationListReceivedListener listener;
	
	@Before
	public void initializeListener(){
		listener = new CliConversationListReceivedListener(new IncrementalIntegerConversationAssociater());
	}
	@Test
	public void onConversationListReceivedTest_NoConversations(){
		listener.onConversationListReceived(new ArrayList<Conversation>());
	}
	
	@Test
	public void onConversationListReceivedTest_WithConversations(){
		User user1 = new User(new UserInfo("name"), new UserId("user1"));
		User user2 = new User(new UserInfo("other name"), new UserId("user2"));
		InMemoryConversationIdGenerator convGenerator = new InMemoryConversationIdGenerator();
		ConversationProperties convProps = new ConversationProperties(new ArrayList<User>(Arrays.asList(user1, user2)), "TestConversation", true);
		
		List<Conversation> conversations = new ArrayList<Conversation>();
		conversations.add(new Conversation(convGenerator.generateConversationId(), convProps));
		listener.onConversationListReceived(conversations);
	}
}

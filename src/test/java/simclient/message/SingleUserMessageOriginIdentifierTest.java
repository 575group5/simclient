package simclient.message;

import org.junit.Assert;
import org.junit.Test;

import simcommon.entities.User;
import simcommon.entities.UserId;
import simcommon.entities.UserInfo;
import simcommon.messages.userMessages.origin.IUserMessageOrigin;
import simcommon.messages.userMessages.origin.ServerOrigin;
import simcommon.messages.userMessages.origin.UserOrigin;

public class SingleUserMessageOriginIdentifierTest {

	@Test
	public void TestGetOriginIdentifier_UserAsOrigin(){
		IUserMessageOrigin origin = new UserOrigin(new User(new UserInfo("username1"), new UserId("user1")));
		SingleUserMessageOriginIdentifier identifier = new SingleUserMessageOriginIdentifier();
		String username = identifier.getOriginIdentifier(origin);
		Assert.assertEquals("username1", username);
	}
	
	@Test
	public void TestGetOriginIdentifier_ServerAsOrigin(){
		IUserMessageOrigin origin = new ServerOrigin();
		SingleUserMessageOriginIdentifier identifier = new SingleUserMessageOriginIdentifier();
		String username = identifier.getOriginIdentifier(origin);
		Assert.assertEquals("Server", username);
	}
	
	@Test
	public void TestGetOriginIdentifier_UnknownOrigin(){
		IUserMessageOrigin origin = null;
		SingleUserMessageOriginIdentifier identifier = new SingleUserMessageOriginIdentifier();
		String username = identifier.getOriginIdentifier(origin);
		Assert.assertEquals("Unknown", username);
	}
}

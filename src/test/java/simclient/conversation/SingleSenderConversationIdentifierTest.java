package simclient.conversation;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

import simcommon.conversation.Conversation;
import simcommon.conversation.ConversationProperties;
import simcommon.conversation.InMemoryConversationIdGenerator;
import simcommon.entities.User;
import simcommon.entities.UserId;
import simcommon.entities.UserInfo;

public class SingleSenderConversationIdentifierTest {

	private Conversation createConversation(ArrayList<User> users){
		InMemoryConversationIdGenerator convGenerator = new InMemoryConversationIdGenerator();
		ConversationProperties convProps = new ConversationProperties(users, "TestConversation", true);
		
		return new Conversation(convGenerator.generateConversationId(), convProps);
	}
	
	@Test
	public void TestGetConversationIdentifier_WithUsers(){
		ArrayList<User> users = new ArrayList<User>(Arrays.asList(new User(new UserInfo("username1"), new UserId("user1")), new User(new UserInfo("username2"), new UserId("user2"))));
		SingleSenderConversationIdentifier identifier = new SingleSenderConversationIdentifier();
		String convName = identifier.getConversationIdentifier(createConversation(users));
		Assert.assertEquals("(username1, username2)", convName);
	}
	
	@Test
	public void TestGetConversationIdentifier_WithoutUsers(){
		SingleSenderConversationIdentifier identifier = new SingleSenderConversationIdentifier();
		String convName = identifier.getConversationIdentifier(createConversation(new ArrayList<User>()));
		Assert.assertEquals("Unknown", convName);
	}
}

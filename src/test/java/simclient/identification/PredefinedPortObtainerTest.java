package simclient.identification;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import simcommon.Constants;
import simcommon.identification.IPortObtainedCallback;

@RunWith(MockitoJUnitRunner.class)
public class PredefinedPortObtainerTest {

	@InjectMocks
	PredefinedPortObtainer portObtainer;
	
	int mPredefinedPort;
	
	@Test
	public void TestPortNumberRetrieval(){
		portObtainer.obtainPort(new IPortObtainedCallback() {
			@Override
			public void onPortObtained(int port) {
				mPredefinedPort = port;
			}
		});
		
		Assert.assertEquals(mPredefinedPort, Constants.SIM_SERVER_PORT_NUMBER);
	}
}
